import express from "express";
import * as DigimonsController from "./src/controllers/DigimonsController";
import * as PokemonController from "./src/controllers/PokemonController";

export const router = express.Router();

router.get("/newdigimon", (req, res) => {
  res.send(
    '<form action="../../digimons/new"   method="POST">    <label>nombre</label>    <input type="text" placeholder="nombre" name="name" required>   <br> <label>tipo</label>    <input type="text" placeholder="type name" name="type_name" required>  <br>  <label>fuerte contra:</label>    <input type="text" placeholder="type strongAgainst" name="type_strongAgainst" required>  <br>  <label>debil contra:</label>    <input type="text" placeholder="type weakAgainst" name="type_weakAgainst" required>  <br>  <label>url</label>    <input type="text" placeholder="url" name="img" required>  <br>  <input type="submit" value="Submit">    </form>'
  );
});router.get("/newpokemon", (req, res) => {
  res.send(
    '<form action="../../pokemon/new"   method="POST">    <label>nombre</label>    <input type="text" placeholder="nombre" name="name" required>   <br> <label>numero</label>    <input type="text" placeholder="numero" name="number" required>   <br> <label>tipo</label>    <input type="text" placeholder="type name" name="type_name" required>  <br>  <label>fuerte contra:</label>    <input type="text" placeholder="type strongAgainst" name="type_strongAgainst" required>  <br>  <label>debil contra:</label>    <input type="text" placeholder="type weakAgainst" name="type_weakAgainst" required>  <br>  <label>url</label>    <input type="text" placeholder="url" name="img" required>  <br>  <input type="submit" value="Submit">    </form>'
  );
});

router.get("/", (req, res) => {
  res.send("Typescript es lo máximo!");
});

router.get("/digimons", DigimonsController.getAll);
router.get("/digimons/:id", DigimonsController.get);
router.get("/digimons/name/:name", DigimonsController.getByName);
router.get("/digimons/type/:type", DigimonsController.getByType);
router.get("/digimons/fuerte/:weakAgainst", DigimonsController.getByFuerte);
router.get("/digimons/debil/:strongAgainst", DigimonsController.getByDebil);
router.get("/comparacion/digimon/:id1/:id2", DigimonsController.comparacion);
router.post("/digimons/new", DigimonsController.NewDigimon);

router.get("/pokemon", PokemonController.getAll);
router.get("/pokemon/:id", PokemonController.get);
router.get("/pokemon/name/:name", PokemonController.getByName);
router.get("/pokemon/type/:type", PokemonController.getByType);
router.get("/pokemon/fuerte/:weakAgainst", PokemonController.getByFuerte);
router.get("/pokemon/debil/:strongAgainst", PokemonController.getByDebil);
router.get("/comparacion/pokemon/:id1/:id2", PokemonController.comparacion);
router.post("/pokemon/new", PokemonController.NewPokemon);

router.post("/", (req, res) => {
  console.log("Cuerpo:", req.body);
  res.status(200).send(req.body);
});
