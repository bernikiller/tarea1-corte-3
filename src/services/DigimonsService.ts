import { DigimonI } from "../interfaces/DigimonInterfaces";
const db = require("../db/Digimons.json");
const uuid = require('uuid');
const fs = require("fs");
const json_books = fs.readFileSync('src/db/Digimons.json', 'utf-8');
const datos: any = JSON.parse(json_books);

module DigimonsService {
  export function getAll(): Array<DigimonI> {
    const digimons: Array<DigimonI> = db;
    return digimons;
  }
  export function get(id: number): DigimonI {
    const digimons: Array<DigimonI> = db;
    const digimon: Array<DigimonI> = digimons.filter((e) => e.id === id);
    if (digimon.length < 1) {
      throw "No se encontró el digimon";
    }
    return digimon[0];
  }
  export function getByName(name: string): Array<DigimonI> {
    const digimons: Array<DigimonI> = db;
    const matches: Array<DigimonI> = digimons.filter(function (el) {
      return el.name.toLowerCase().indexOf(name.toLowerCase()) > -1;
    });
    if (matches.length < 1) {
      throw "No se encontró el digimon";
    }
    return matches;
  }

  export function getByType(type: string): Array<DigimonI> {
    const digimons: Array<DigimonI> = db;
    let matches: Array<DigimonI> = [];
    digimons.forEach((digimon) => {
      const found = digimon.type.filter((e) => e.name === type);
      if (found.length > 0) {
        matches.push(digimon);
      }
    });

    if (matches.length < 1) {
      throw "No se encontró el tipo";
    }
    return matches;
  }

  export function getByFuerte(type: string): Array<DigimonI> {
    const digimons: Array<DigimonI> = db;
    let matches: Array<DigimonI> = [];
    digimons.forEach((digimon) => {
      const found = digimon.type.filter((e) => e.weakAgainst === type);
      if (found.length > 0) {
        matches.push(digimon);
      }
    });

    if (matches.length < 1) {
      throw "No se encontró el tipo fuerte";
    }
    return matches;
  }

  export function getByDebil(type: string): Array<DigimonI> {
    const digimons: Array<DigimonI> = db;
    let matches: Array<DigimonI> = [];
    digimons.forEach((digimon) => {
      const found = digimon.type.filter((e) => e.strongAgainst === type);
      if (found.length > 0) {
        matches.push(digimon);
      }
    });

    if (matches.length < 1) {
      throw "No se encontró el tipo debil";
    }
    return matches;
  }

  export function NewDigimon(
    name: string,
    type_name: string,
    type_strongAgainst: string,
    type_weakAgainst: string,
    img: string
  ) {
    let type = [{
      name: type_name,
      strongAgainst: type_strongAgainst,
      weakAgainst: type_weakAgainst,
    }];
    let newdato = {
      id: uuid.v4(),
      name,
      type,
      img,
    };
    datos.push(newdato);

    const json_books = JSON.stringify(datos);
    fs.writeFileSync("src/db/Digimons.json", json_books, "utf-8");

    return "Digimon creado";
  }

  export function comparacion(nombre1:string,nombre2:string){
    const digimons: Array<DigimonI> = db;
    const nombred1:any[] = [];
    const nombred2:any[] = [];
    const digimon2: Array<DigimonI> = digimons.filter(function (el) {
      if(el.name.toLowerCase().indexOf(nombre2.toLowerCase()) > -1){
        nombred2.push(el.name);
      }
      return el.name.toLowerCase().indexOf(nombre2.toLowerCase()) > -1;
    });
    
    const digimon1: Array<DigimonI> = digimons.filter(function (el) {
      if(el.name.toLowerCase().indexOf(nombre1.toLowerCase()) > -1){
        nombred1.push(el.name);
      }
      return el.name.toLowerCase().indexOf(nombre1.toLowerCase()) > -1;
    });
    if (digimon1.length < 1 && digimon2.length < 1) {
      throw "No se encontraron los digimons";
    }
    
    const fuerte:any[] = [];
    const debil:any[] = [];
    const types:any[] = [];
    const respuesta:any[] = [];
    digimon1.forEach(digi=>{
      digi.type.forEach(tipo=>{
        fuerte.push(tipo.strongAgainst.toLowerCase());
      });
    });
    digimon1.forEach(digi=>{
      digi.type.forEach(tipo=>{
        debil.push(tipo.weakAgainst.toLowerCase());
      });
    });
    digimon2.forEach(digi=>{
      digi.type.forEach(tipo=>{
        types.push(tipo.name.toLowerCase());
      });
    });
    if(types[0]==fuerte[0]){
      respuesta.push(nombred1+" es fuerte contra "+nombred2);
    }else if(types[0]==debil[0]){
      respuesta.push(nombred1+" es debil contra "+nombred2);
    }else{
      respuesta.push(nombred1+" es el mismo tipo que "+nombred2);
    }
    return respuesta[0].toString();
  }
}

export default DigimonsService;
