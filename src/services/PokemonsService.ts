import { PokemonI } from "../interfaces/PokemonInterfaces";
const db = require("../db/Pokemons.json");
const uuid = require('uuid');
const fs = require("fs");
const json_books = fs.readFileSync('src/db/Pokemons.json', 'utf-8');
const datos: any = JSON.parse(json_books);

module PokemonsService {
  export function getAll(): Array<PokemonI> {
    const pokemons: Array<PokemonI> = db;
    return pokemons;
  }
  export function get(id: number): PokemonI {
    const pokemons: Array<PokemonI> = db;
    const pokemon: Array<PokemonI> = pokemons.filter((e) => e.id === id);
    if (pokemon.length < 1) {
      throw "No se encontró el pokemon";
    }
    return pokemon[0];
  }
  export function getByName(name: string): Array<PokemonI> {
    const pokemons: Array<PokemonI> = db;
    const matches: Array<PokemonI> = pokemons.filter(function (el) {
      return el.name.toLowerCase().indexOf(name.toLowerCase()) > -1;
    });
    if (matches.length < 1) {
      throw "No se encontró el pokemon";
    }
    return matches;
  }

  export function getByType(type: string): Array<PokemonI> {
    const pokemons: Array<PokemonI> = db;
    let matches: Array<PokemonI> = [];
    pokemons.forEach((pokemon) => {
      const found = pokemon.type.filter((e) => e.name === type);
      if (found.length > 0) {
        matches.push(pokemon);
      }
    });

    if (matches.length < 1) {
      throw "No se encontró el tipo";
    }
    return matches;
  }

  export function getByFuerte(type: string): Array<PokemonI> {
    const pokemons: Array<PokemonI> = db;
    let matches: Array<PokemonI> = [];
    pokemons.forEach((pokemon) => {
      const found = pokemon.type.filter((e) => e.weakAgainst === type);
      if (found.length > 0) {
        matches.push(pokemon);
      }
    });

    if (matches.length < 1) {
      throw "No se encontró el tipo fuerte";
    }
    return matches;
  }

  export function getByDebil(type: string): Array<PokemonI> {
    const pokemons: Array<PokemonI> = db;
    let matches: Array<PokemonI> = [];
    pokemons.forEach((pokemon) => {
      const found = pokemon.type.filter((e) => e.strongAgainst === type);
      if (found.length > 0) {
        matches.push(pokemon);
      }
    });

    if (matches.length < 1) {
      throw "No se encontró el tipo debil";
    }
    return matches;
  }

  export function NewPokemon(
    name: string,
    number: string,
    type_name: string,
    type_strongAgainst: string,
    type_weakAgainst: string,
    img: string
  ) {
    let type = [{
      name: type_name,
      strongAgainst: type_strongAgainst,
      weakAgainst: type_weakAgainst,
    }];
    let newdato = {
      id: uuid.v4(),
      name,
      number,
      type,
      img,
    };
    datos.push(newdato);

    const json_books = JSON.stringify(datos);
    fs.writeFileSync("src/db/pokemons.json", json_books, "utf-8");

    return "Pokemon creado";
  }
  
  export function comparacion(nombre1:string,nombre2:string){
    const pokemons: Array<PokemonI> = db;
    const nombred1:any[] = [];
    const nombred2:any[] = [];
    const pokemon2: Array<PokemonI> = pokemons.filter(function (el) {
      if(el.name.toLowerCase().indexOf(nombre2.toLowerCase()) > -1){
        nombred2.push(el.name);
      }
      return el.name.toLowerCase().indexOf(nombre2.toLowerCase()) > -1;
    });
    
    const pokemon1: Array<PokemonI> = pokemons.filter(function (el) {
      if(el.name.toLowerCase().indexOf(nombre1.toLowerCase()) > -1){
        nombred1.push(el.name);
      }
      return el.name.toLowerCase().indexOf(nombre1.toLowerCase()) > -1;
    });
    if (pokemon1.length < 1 && pokemon2.length < 1) {
      throw "No se encontraron los pokemons";
    }
    
    const fuerte:any[] = [];
    const debil:any[] = [];
    const types:any[] = [];
    const respuesta:any[] = [];
    pokemon1.forEach(digi=>{
      digi.type.forEach(tipo=>{
        fuerte.push(tipo.strongAgainst.toLowerCase());
      });
    });
    pokemon1.forEach(digi=>{
      digi.type.forEach(tipo=>{
        debil.push(tipo.weakAgainst.toLowerCase());
      });
    });
    pokemon2.forEach(digi=>{
      digi.type.forEach(tipo=>{
        types.push(tipo.name.toLowerCase());
      });
    });
    if(types[0]==fuerte[0]){
      respuesta.push(nombred1+" es fuerte contra "+nombred2);
    }else if(types[0]==debil[0]){
      respuesta.push(nombred1+" es debil contra "+nombred2);
    }else{
      respuesta.push(nombred1+" es el mismo tipo que "+nombred2);
    }
    return respuesta[0].toString();
  }
}

export default PokemonsService;
