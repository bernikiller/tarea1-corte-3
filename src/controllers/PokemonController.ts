import { Request, Response } from "express";
import PokemonsService from "../services/PokemonsService";

export function getAll(_: any, res: Response) {
  const pokemons = PokemonsService.getAll();
  res.status(200).json(pokemons);
}

export function get(req: Request, res: Response) {
    try {
      const id = (req.params.id && +req.params.id) || undefined;
      if (!id) {
        throw "Se requiere el ID del pokemon.";
      }
      const pokemon = PokemonsService.get(id);
      res.status(200).json(pokemon);
    } catch (error) {
      res.status(400).send(error);
    }
  }
  
  export function getByName(req: Request, res: Response) {
    try {
      const name = (req.params.name && req.params.name) || undefined;
      if (!name) {
        throw "Se requiere el name del pokemon.";
      }
      const pokemon = PokemonsService.getByName(name);
      res.status(200).json(pokemon);
    } catch (error) {
      res.status(400).send(error);
    }
  }
  
  export function getByType(req: Request, res: Response) {
    try {
      const type = (req.params.type && req.params.type) || undefined;
      if (!type) {
        throw "Se requiere el Tipo del pokemon.";
      }
      const pokemon = PokemonsService.getByType(type);
      res.status(200).json(pokemon);
    } catch (error) {
      res.status(400).send(error);
    }
  }
  
  export function getByFuerte(req: Request, res: Response) {
    try {
      const weakAgainst =
        (req.params.weakAgainst && req.params.weakAgainst) || undefined;
      if (!weakAgainst) {
        throw "Se requiere el tipo del pokemon.";
      }
      const pokemon = PokemonsService.getByFuerte(weakAgainst);
      res.status(200).json(pokemon);
    } catch (error) {
      res.status(400).send(error);
    }
  }
  
  export function getByDebil(req: Request, res: Response) {
    try {
      const strongAgainst =
        (req.params.strongAgainst && req.params.strongAgainst) || undefined;
      if (!strongAgainst) {
        throw "Se requiere el tipo del pokemon.";
      }
      const pokemon = PokemonsService.getByDebil(strongAgainst);
      res.status(200).json(pokemon);
    } catch (error) {
      res.status(400).send(error);
    }
  }
  
  export function NewPokemon(req: Request, res: Response) {
    try {
      const {
        name,
        number,
        type_name,
        type_strongAgainst,
        type_weakAgainst,
        img,
      } = req.body;
      if (
        !name ||
        !number ||
        !type_name ||
        !type_strongAgainst ||
        !type_weakAgainst ||
        !img
      ) {
        throw "Se requiere todos los datos.";
      }
      const pokemon = PokemonsService.NewPokemon(
        name,
        number,
        type_name,
        type_strongAgainst,
        type_weakAgainst,
        img
      );
  
      res.status(200).json(pokemon);
    } catch (error) {
      res.status(400).send(error);
    }
  }
  
export function comparacion(req: Request, res: Response) {
  try {
    const id1 = (req.params.id1 && req.params.id1) || undefined;
    const id2 = (req.params.id2 && req.params.id2) || undefined;
    if (!id1 || !id2) {
      throw "Se requiere todos los datos.";
    }
    const digimons = PokemonsService.comparacion(id1, id2);

    res.status(200).json(digimons.toString().replace(/['"]+/g, ''));
  } catch (error) {
    res.status(400).send(error);
  }
}