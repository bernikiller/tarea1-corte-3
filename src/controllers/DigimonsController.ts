import { Request, Response } from "express";
import DigimonsService from "../services/DigimonsService";

export function getAll(_: any, res: Response) {
  const digimons = DigimonsService.getAll();
  res.status(200).json(digimons);
}

export function get(req: Request, res: Response) {
  try {
    const id = (req.params.id && +req.params.id) || undefined;
    if (!id) {
      throw "Se requiere el ID del digimon.";
    }
    const digimon = DigimonsService.get(id);
    res.status(200).json(digimon);
  } catch (error) {
    res.status(400).send(error);
  }
}

export function getByName(req: Request, res: Response) {
  try {
    const name = (req.params.name && req.params.name) || undefined;
    if (!name) {
      throw "Se requiere el name del digimon.";
    }
    const digimons = DigimonsService.getByName(name);
    res.status(200).json(digimons);
  } catch (error) {
    res.status(400).send(error);
  }
}

export function getByType(req: Request, res: Response) {
  try {
    const type = (req.params.type && req.params.type) || undefined;
    if (!type) {
      throw "Se requiere el Tipo del digimon.";
    }
    const digimons = DigimonsService.getByType(type);
    res.status(200).json(digimons);
  } catch (error) {
    res.status(400).send(error);
  }
}

export function getByFuerte(req: Request, res: Response) {
  try {
    const weakAgainst =
      (req.params.weakAgainst && req.params.weakAgainst) || undefined;
    if (!weakAgainst) {
      throw "Se requiere el tipo del digimon.";
    }
    const digimons = DigimonsService.getByFuerte(weakAgainst);
    res.status(200).json(digimons);
  } catch (error) {
    res.status(400).send(error);
  }
}

export function getByDebil(req: Request, res: Response) {
  try {
    const strongAgainst =
      (req.params.strongAgainst && req.params.strongAgainst) || undefined;
    if (!strongAgainst) {
      throw "Se requiere el tipo del digimon.";
    }
    const digimons = DigimonsService.getByDebil(strongAgainst);
    res.status(200).json(digimons);
  } catch (error) {
    res.status(400).send(error);
  }
}

export function NewDigimon(req: Request, res: Response) {
  try {
    const {
      name,
      type_name,
      type_strongAgainst,
      type_weakAgainst,
      img,
    } = req.body;
    if (
      !name ||
      !type_name ||
      !type_strongAgainst ||
      !type_weakAgainst ||
      !img
    ) {
      throw "Se requiere todos los datos.";
    }
    const digimons = DigimonsService.NewDigimon(
      name,
      type_name,
      type_strongAgainst,
      type_weakAgainst,
      img
    );

    res.status(200).json(digimons);
  } catch (error) {
    res.status(400).send(error);
  }
}

export function comparacion(req: Request, res: Response) {
  try {
    const id1 = (req.params.id1 && req.params.id1) || undefined;
    const id2 = (req.params.id2 && req.params.id2) || undefined;
    if (!id1 || !id2) {
      throw "Se requiere todos los datos.";
    }
    const digimons = DigimonsService.comparacion(id1, id2);

    res.status(200).json(digimons.toString().replace(/['"]+/g, ''));
  } catch (error) {
    res.status(400).send(error);
  }
}
