* Servicio para consultar por nombre (puedo no ponerlo completo y puedo no usar tilde)
* Servicio para consultar por tipo
* Servicio para consultar si un digimón/pokemón es fuerte o débil contra otro
* /digimons/fuerte/:weakAgainst => ingresar el tipo a consultar
* /digimons/debil/:strongAgainst => ingresar el tipo a consultar
* /pokemon/fuerte/:weakAgainst => ingresar el tipo a consultar
* /pokemon/debil/:strongAgainst => ingresar el tipo a consultar
* /comparacion/pokemon/:nombrepokemon1/:nombrepokemon2 => ingresar los nombre de los pokemons a consultar
* /comparacion/digimon/:nombredigimon1/:nombredigimon2 => ingresar los nombre de los digimons a consultar

* Servicio para crear un nuevo digimon/pokemón (que estará creado en memoria, no debe persistir aún)
* /newdigimon => rellenar todo los campos y enviarlos
* /newpokemon => rellenar todo los campos y enviarlos
